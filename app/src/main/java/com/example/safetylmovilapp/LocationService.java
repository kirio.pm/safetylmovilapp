package com.example.safetylmovilapp;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class LocationService extends Service {
    private LocationCallback locationCallback= new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            if(locationResult !=null && locationResult.getLastLocation() != null){
                double latitud=locationResult.getLastLocation().getLatitude();
                double longitud=locationResult.getLastLocation().getLongitude();
                Log.d("UBicacion", latitud + ", "+ longitud);
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new  UnsupportedOperationException("No se pudo implementar");
    }

    @SuppressLint("MissingPermission")
    private void startLocaionService(){
            String channelId="location_notification_channel";
        NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent= new Intent();
        PendingIntent pendingIntent= PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder= new NotificationCompat.Builder(getApplicationContext(), channelId);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("Safetyl Ubicacion ");
        builder.setDefaults(NotificationCompat.DEFAULT_ALL);
        builder.setContentText("Iniciiando...");
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(false);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            if (notificationManager!=null && notificationManager.getNotificationChannel(channelId)==null){
                NotificationChannel notificationChannel= new NotificationChannel(channelId, "Servicio de Ubicacion", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setDescription("Este canal ha sido utilizado por Location service");
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        LocationRequest locationRequest= new LocationRequest();
        locationRequest.setInterval(4000);
        locationRequest.setFastestInterval(2000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        startForeground(Constants.LOCATION_SERVICE_ID, builder.build());
    }

    private void stopServiceLocation(){
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback);
        stopForeground(true);
        stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent!=null){
            String action=intent.getAction();
            if (action!=null){
                if (action.equals(Constants.ACTION_START_LOCATION_SERVICE)){
                    startLocaionService();
                }else if(action.equals(Constants.ACTION_STOP_LOCATION_SERVICE)){
                    stopServiceLocation();
                }
            }

        }
        return super.onStartCommand(intent, flags, startId);
    }
}

